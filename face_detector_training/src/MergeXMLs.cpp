#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <sstream>
#include <vector>
#include <dlib/image_io.h>
#include <dlib/image_processing.h>
#include "XMLParser.h"

class TrainingXMLMerger
{
public:
	TrainingXMLMerger(
		const std::string& catalogFolderName,
		const std::string& catalogName, 
		const std::string& inXMLBaseName, 
		const std::string& outXMLName);

	~TrainingXMLMerger();
	void MergeAll();

private:
	enum class State
	{
		Initial,
		GetFolderName,
		GetBatches
	};
	
	std::string m_catalogFolderName;
	std::string m_inXMLBaseName;
	std::string m_outXMLName;

	std::string m_outFolder;

	std::vector<folder_tag> m_inFolders;

	std::ofstream m_outXML;
	State m_state = State::Initial;

	void MergeFolder(int folderId);

   	dlib::rectangle InflateROI(const dlib::rectangle& roi, int imgWidth, int imgHeight, int inflationPercentage) const;
	dlib::array2d<dlib::rgb_pixel> CropImage(image_tag& imgTag, const std::string& inImageFolder, int inflationPercentage) const;
};

TrainingXMLMerger::TrainingXMLMerger(
	const std::string& catalogFolderName,
	const std::string& catalogName, 
	const std::string& inXMLBaseName, 
	const std::string& outXMLName)
: m_catalogFolderName(catalogFolderName)
, m_inXMLBaseName(inXMLBaseName)
, m_outXMLName(outXMLName)
{
	if(m_catalogFolderName.back() == '/')
		m_catalogFolderName.pop_back();

	m_outFolder = m_catalogFolderName + "/merged";

	ManifestXMLHandler handler;
	
	auto catalogFullPath = m_catalogFolderName + "/" + catalogName + ".xml";

	parse_xml(catalogFullPath, handler);
	m_inFolders = handler.Get();

	std::string outXMLFullPath = m_outFolder + "/" + m_outXMLName + ".xml";
	m_outXML.open(outXMLFullPath, std::ios::trunc);
	if(!m_outXML.is_open())
	{
		std::stringstream errMsg;
		errMsg << "Could not open output xml file " << outXMLFullPath;
		throw std::runtime_error(errMsg.str());
	}

	m_outXML << "<?xml version='1.0' encoding='ISO-8859-1'?>"<<std::endl;
	m_outXML << "<?xml-stylesheet type='text/xsl' href='image_metadata_stylesheet.xsl'?>"<<std::endl;
	m_outXML << "<dataset>"<<std::endl;
	m_outXML << "<name>Training faces</name>"<<std::endl;
	m_outXML << "<comment>These are images from the PASCAL VOC 2011 dataset.</comment>"<<std::endl;
	m_outXML << "<images>"<<std::endl;
}

TrainingXMLMerger::~TrainingXMLMerger()
{
	if(m_outXML.is_open())
	{
		m_outXML << "</images>" <<std::endl;
		m_outXML << "</dataset>" <<std::endl;

		m_outXML.close();
	}
}

void TrainingXMLMerger::MergeAll()
{
	for(int i = 0; i < m_inFolders.size(); ++i)
		MergeFolder(i);
}

void TrainingXMLMerger::MergeFolder(int folderId)
{
	int numBatches = m_inFolders.at(folderId).segments.size();
	auto imgDir = m_catalogFolderName + "/" + m_inFolders.at(folderId).folder_path;

	for(int i = 0; i < numBatches; ++i)
	{
		FacialLandmarkXMLHandler handler;

		std::stringstream inputXMLName;
		inputXMLName << imgDir << "/" << m_inXMLBaseName << "_"<< m_inFolders.at(folderId).segments[i] << ".xml";

		parse_xml(inputXMLName.str(), handler);

		auto imageTags = handler.Get();

		for(auto mapPair: imageTags)
		{
			auto imgTag = mapPair.second;
			auto croppedImg = CropImage(imgTag, imgDir, m_inFolders.at(folderId).roi_inflation_percentage);

			imgTag.Validate(m_inFolders.at(folderId).expected_num_landmarks, m_inFolders.at(folderId).max_num_boxes);

			dlib::save_png(croppedImg, imgTag.file_path);
			m_outXML<<imgTag;
		}
	}
}

dlib::rectangle TrainingXMLMerger::InflateROI(const dlib::rectangle& roi, int imgWidth, int imgHeight, int inflationPercentage) const
{
    const float rate = 1.0f+inflationPercentage/100.0f;

    const int centerX = (roi.left() + roi.right())/2;
    const int centerY = (roi.top() + roi.bottom())/2;

    int left = centerX - std::lround((roi.width()/2.0)*rate);
    int right = centerX + std::lround((roi.width()/2.0)*rate);

    int top = centerY - std::lround((roi.height()/2.0)*rate);
    int bottom = centerY + std::lround((roi.height()/2.0)*rate);

    left = std::min(std::max(left, 0), imgWidth - 1);
    right = std::min(std::max(right, 0), imgWidth - 1);

    top = std::min(std::max(top, 0), imgHeight - 1);
    bottom = std::min(std::max(bottom, 0), imgHeight - 1);

    return dlib::rectangle(left, top, right, bottom);
}

dlib::array2d<dlib::rgb_pixel> TrainingXMLMerger::CropImage(image_tag& imgTag, const std::string& inImageFolder, int inflationPercentage) const
{
	const std::string inImagePath = inImageFolder+"/"+imgTag.file_path;

	dlib::array2d<dlib::rgb_pixel> fullImg;
	dlib::load_image(fullImg, inImagePath.c_str());

	const int imgWidth = static_cast<int>(fullImg.nc());
	const int imgHeight = static_cast<int>(fullImg.nr());

	auto roi = InflateROI(imgTag.FindTightestROI(), imgWidth, imgHeight, inflationPercentage);
	int dx = -static_cast<int>(roi.left());
	int dy = -static_cast<int>(roi.top());

	dlib::array2d<dlib::rgb_pixel> croppedImg;
	dlib::assign_image(croppedImg, dlib::sub_image(fullImg, roi));

	auto imgNameNoExt = imgTag.file_path.substr(0, imgTag.file_path.length()-4);

	imgTag.file_path = m_outFolder + "/" + imgNameNoExt + ".png";
	imgTag.ShiftCoordinates(dx, dy);

	return croppedImg;
}

int main(int argc, char** argv) try
{
	const char* usage = 
		"Usage: ./MergeXMLs "
		"\n\tpath-to-image-catalog(string) "
		"\n\tcatalog-name-no-extension(string) "
		"\n\tinput-xml-base-name-no-extension(string) "
		"\n\tmerged-xml-name-no-extension(string) ";

	if (argc != 5) 
		throw std::runtime_error(usage);  

	TrainingXMLMerger merger(argv[1], argv[2], argv[3], argv[4]);  
	merger.MergeAll();	

}
catch(std::exception& e)
{
    std::cout << e.what() << std::endl;
}
