#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <iostream>
#include "XMLParser.h"

class TrainingXMLWriter
{
public:
	TrainingXMLWriter(
		const std::string& imgDir, 
		const std::string& inFileName, 
		const std::string& bboxFileName, 
		const std::string& outFileName,
		int numBatches);
	void ProcessAllImages();

private:
	std::string m_imgDir;
	std::string m_inFileName;
	std::string m_bboxFileName;
	std::string m_outFileName;
	int m_numBatches;

	std::vector<std::ofstream> m_outputXMLs;

	void StartNewFile(unsigned int batchId);
	void CloseFile(unsigned int batchId);
	void ProcessBatch(unsigned int batchId);
};

TrainingXMLWriter::TrainingXMLWriter(
	const std::string& imgDir, 
	const std::string& inFileName, 
	const std::string& bboxFileName, 
	const std::string& outFileName, 
	int numBatches)
: m_imgDir(imgDir)
, m_inFileName(inFileName)
, m_bboxFileName(bboxFileName)
, m_outFileName(outFileName)
, m_numBatches(numBatches)
{
	m_outputXMLs.resize(numBatches);
}

void TrainingXMLWriter::ProcessAllImages()
{
	for(int batchId = 0; batchId < m_numBatches; ++batchId)
		ProcessBatch(batchId);
}

void TrainingXMLWriter::ProcessBatch(unsigned int batchId)
{
	FacialLandmarkXMLHandler inputHandler;
	FacialLandmarkXMLHandler bboxHandler;

	std::stringstream inputXMLName;
	inputXMLName << m_imgDir << "/" << m_inFileName << "_"<< batchId << ".xml";

	dlib::parse_xml(inputXMLName.str(), inputHandler);

	std::stringstream bboxXMLName;
	bboxXMLName << m_imgDir << "/" << m_bboxFileName << "_"<< batchId << ".xml";

	dlib::parse_xml(bboxXMLName.str(), bboxHandler);

	const auto& imageTagsInput = inputHandler.Get();
	const auto& imageTagsBbox = bboxHandler.Get();

	StartNewFile(batchId);
	for(auto it = imageTagsInput.begin(); it != imageTagsInput.end(); ++it)
	{
		auto imgTag = it->second;
		auto found = imageTagsBbox.find(imgTag.file_path);
		if(found != imageTagsBbox.end())
		{
			auto top = (found->second).boxes.at(0).top;
			auto left = (found->second).boxes.at(0).left;
			auto width = (found->second).boxes.at(0).width;
			auto height = (found->second).boxes.at(0).height;

			image_tag newTag;
			newTag.file_path = imgTag.file_path;
			newTag.boxes.push_back(imgTag.boxes.at(0));
			newTag.boxes.at(0).top = top;
			newTag.boxes.at(0).left = left;
			newTag.boxes.at(0).width = width;
			newTag.boxes.at(0).height = height;

			m_outputXMLs[batchId]<<newTag;
		}
	}
	CloseFile(batchId);
}

void TrainingXMLWriter::StartNewFile(unsigned int batchId)
{
	std::stringstream fullOutputName;
	fullOutputName << m_imgDir << "/" << m_outFileName << "_" << batchId << ".xml";
	m_outputXMLs[batchId].open(fullOutputName.str(), std::ios::trunc);
	
	if(!m_outputXMLs[batchId].is_open())
	{
		std::stringstream errMsg;
		errMsg << "Could not open output xml file " << fullOutputName.str();
		throw std::runtime_error(errMsg.str());
	}

	m_outputXMLs[batchId] << "<?xml version='1.0' encoding='ISO-8859-1'?>"<<std::endl;
	m_outputXMLs[batchId] << "<?xml-stylesheet type='text/xsl' href='image_metadata_stylesheet.xsl'?>"<<std::endl;
	m_outputXMLs[batchId] << "<dataset>"<<std::endl;
	m_outputXMLs[batchId] << "<name>Training faces</name>"<<std::endl;
	m_outputXMLs[batchId] << "<comment>These are images from the PASCAL VOC 2011 dataset.</comment>"<<std::endl;
	m_outputXMLs[batchId] << "<images>"<<std::endl;
}

void TrainingXMLWriter::CloseFile(unsigned int batchId)
{
	if(m_outputXMLs[batchId].is_open())
	{
		m_outputXMLs[batchId] << "</images>" <<std::endl;
		m_outputXMLs[batchId] << "</dataset>" <<std::endl;
		m_outputXMLs[batchId].close();
	}
}

int main(int argc, char** argv) try
{
	const char* usage = 
		"Usage: ./FixBBox "
		"\n\tpath-to-image-folder(string) "
		"\n\tinput-label-file-without-extension(string) "
		"\n\tbbox-only-file-without-extension(string) "
		"\n\toutput-label-file-without-extension(string) "
		"\n\t#-of-batches(int) ";

	if (argc != 6) 
		throw std::runtime_error(usage);  

	TrainingXMLWriter xmlOutput(argv[1], argv[2], argv[3], argv[4], std::atoi(argv[5]));
	xmlOutput.ProcessAllImages();
}
catch(std::exception& e)
{
    std::cout << e.what() << std::endl;
}

