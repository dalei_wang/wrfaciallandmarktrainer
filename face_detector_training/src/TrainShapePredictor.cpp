#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing.h>
#include <dlib/console_progress_indicator.h>
#include <dlib/data_io.h>
#include <dlib/statistics.h>
#include <iostream>
#include <cstdlib>
#include "FacialLandmarkDefinition.h"

using namespace dlib;
using namespace std;

// ----------------------------------------------------------------------------------------

std::vector<std::vector<double> > get_interocular_distances (
    const std::vector<std::vector<full_object_detection> >& objects
);

// ----------------------------------------------------------------------------------------

template <
    typename image_array_type,
    typename T
    >
void add_image_left_right_flips_20points (
    image_array_type& images,
    std::vector<std::vector<T> >& objects
)
{
    // make sure requires clause is not broken
    DLIB_ASSERT( images.size() == objects.size(),
        "\t void add_image_left_right_flips()"
        << "\n\t Invalid inputs were given to this function."
        << "\n\t images.size():  " << images.size() 
        << "\n\t objects.size(): " << objects.size() 
        );

    typename image_array_type::value_type temp;
    std::vector<T> rects;

    const unsigned long num = images.size();
    for (unsigned long j = 0; j < num; ++j)
    {
        const point_transform_affine tran = flip_image_left_right(images[j], temp);

        rects.clear();
        for (unsigned long i = 0; i < objects[j].size(); ++i)
        {
            rects.push_back(impl::tform_object(tran, objects[j][i]));

            DLIB_CASSERT(rects.back().num_parts() == 20);
            swap(rects.back().part(FacialLandMark::lleyebrow), rects.back().part(FacialLandMark::rreyebrow));
            swap(rects.back().part(FacialLandMark::lreyebrow), rects.back().part(FacialLandMark::rleyebrow));
            swap(rects.back().part(FacialLandMark::lmeyebrow), rects.back().part(FacialLandMark::rmeyebrow));
            swap(rects.back().part(FacialLandMark::lleye), rects.back().part(FacialLandMark::rreye));
            swap(rects.back().part(FacialLandMark::lreye), rects.back().part(FacialLandMark::rleye));
            swap(rects.back().part(FacialLandMark::lmouth), rects.back().part(FacialLandMark::rmouth));
            swap(rects.back().part(FacialLandMark::lcheek), rects.back().part(FacialLandMark::rcheek));
            swap(rects.back().part(FacialLandMark::lmcheek), rects.back().part(FacialLandMark::rmcheek));
        }

        images.push_back(temp);
        objects.push_back(rects);
    }
}

// ----------------------------------------------------------------------------------------

int main(int argc, char** argv)
{
    try
    {
        if (argc != 2)
        {
            cout << "give the path to the training data folder" << endl;
            return 0;
        }
        const std::string faces_directory = argv[1];
        dlib::array<array2d<unsigned char> > images_train, images_test, raw_images_train;
        std::vector<std::vector<full_object_detection> > faces_train, faces_test, raw_faces_train;

        std::vector<std::string> parts_list;
        //load_image_dataset(raw_images_train, raw_faces_train, faces_directory+"/training.xml", parts_list);
		load_image_dataset(images_train, faces_train, faces_directory+"/training.xml", parts_list);
        cout << "Training Dataset Loaded" << endl;

		/*
        if(raw_faces_train.size() != raw_images_train.size());
            throw std::runtime_error("raw_faces_train.size() != raw_images_train.size()");

        const int acceptanceThreshold = 50;
        srand(12345);
        for(int i = 0; i < raw_faces_train.size(); ++i)
        {
            if(std::rand()%100 < acceptanceThreshold)
            {
                images_train.push_back(raw_images_train[i]);
                faces_train.push_back(raw_faces_train[i]);
            }
        }

        raw_images_train.clear();
        raw_faces_train.clear();     
		*/

        load_image_dataset(images_test, faces_test, faces_directory+"/test.xml");
        cout << "Testing Dataset Loaded" << endl;

        add_image_left_right_flips_20points(images_train, faces_train);
        cout << "Applied left-right flip to training dataset" << endl;
        
        add_image_left_right_flips_20points(images_test, faces_test);
        cout << "Applied left-right flip to test dataset" << endl;
        
        add_image_rotations(linspace(-30,30,5)*pi/180.0,images_train, faces_train);
        cout << "Applied rotation to training dataset" << endl;

        cout << "num training images: "<< images_train.size() << endl;

        for (auto& part : parts_list)
            cout << part << endl;

        shape_predictor_trainer trainer;
        trainer.set_oversampling_amount(40);
        trainer.set_num_test_splits(150);
        trainer.set_feature_pool_size(800);
        trainer.set_num_threads(4);
        trainer.set_cascade_depth(15);
        trainer.be_verbose();

        // Now finally generate the shape model
        shape_predictor sp = trainer.train(images_train, faces_train);

        serialize("shape_predictor_20_face_landmarks_v4.dat") << sp;

        cout << "mean training error: "<< 
            test_shape_predictor(sp, images_train, faces_train, get_interocular_distances(faces_train)) << endl;

        cout << "mean testing error:  "<< 
            test_shape_predictor(sp, images_test, faces_test, get_interocular_distances(faces_test)) << endl;

    }
    catch (exception& e)
    {
        cout << "\nexception thrown!" << endl;
        cout << e.what() << endl;
    }
}

// ----------------------------------------------------------------------------------------

double interocular_distance (
    const full_object_detection& det
)
{
    dlib::vector<double,2> l, r;
    // left eye
    l = (det.part(FacialLandMark::lleye) + det.part(FacialLandMark::lreye))/2;
    // right eye
    r = (det.part(FacialLandMark::rreye) + det.part(FacialLandMark::rleye))/2;

    return length(l-r);
}

std::vector<std::vector<double> > get_interocular_distances (
    const std::vector<std::vector<full_object_detection> >& objects
)
{
    std::vector<std::vector<double> > temp(objects.size());
    for (unsigned long i = 0; i < objects.size(); ++i)
    {
        for (unsigned long j = 0; j < objects[i].size(); ++j)
        {
            temp[i].push_back(interocular_distance(objects[i][j]));
        }
    }
    return temp;
}

// ----------------------------------------------------------------------------------------
