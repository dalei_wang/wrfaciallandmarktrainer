#include <dlib/xml_parser.h>
#include <dlib/geometry.h>
#include <vector>
#include <iostream>
#include <unordered_map>
#include <numeric>

struct part_tag
{
    int x;
    int y;
    std::string name;
    void ShiftCoordinates(int dx, int dy);
};

std::ostream& operator<<(std::ostream& os, const part_tag& tag);

struct box_tag
{
    int top;
    int left;
    int width;
    int height;
    std::vector<part_tag> parts;

    bool Validate(std::string file_path, int expectedNumLandmarks) const;
    dlib::rectangle FindTightestROI() const;
    void ShiftCoordinates(int dx, int dy);
};

std::ostream& operator<<(std::ostream& os, const box_tag& tag);

struct image_tag
{
    std::string file_path;
    std::vector<box_tag> boxes;

    bool Validate(int expectedNumLandmarks, int maxNumBoxes) const;
    dlib::rectangle FindTightestROI() const;
    void ShiftCoordinates(int dx, int dy);
};

std::ostream& operator<<(std::ostream& os, const image_tag& tag);

class FacialLandmarkXMLHandler : public dlib::document_handler
{
public:
    const std::unordered_map<std::string, image_tag>& Get() const;

    virtual void start_document ()
    {}

    virtual void end_document ()
    {}

    virtual void start_element ( 
        const unsigned long line_number,
        const std::string& name,
        const dlib::attribute_list& atts);
 
    virtual void end_element ( 
        const unsigned long line_number,
        const std::string& name);

    virtual void characters ( 
        const std::string& data)
    {}

    virtual void processing_instruction (
        const unsigned long line_number,
        const std::string& target,
        const std::string& data)
    {}

private:
    std::unordered_map<std::string, image_tag> m_images;
    std::unordered_map<std::string, image_tag>::iterator m_currentImage;
};

struct folder_tag
{
    std::string folder_path;
    int roi_inflation_percentage;
    int max_num_boxes;
    int expected_num_landmarks;
    std::vector<int> segments; 
};

class ManifestXMLHandler : public dlib::document_handler
{
public:

    const std::vector<folder_tag>& Get() const
    {
        return m_folders;
    }

    virtual void start_document ()
    {}

    virtual void end_document ()
    {}

    virtual void start_element ( 
        const unsigned long line_number,
        const std::string& name,
        const dlib::attribute_list& atts)
    {
        if(name == "folder")
        {           
            m_folders.emplace_back();
            m_folders.back().max_num_boxes = std::numeric_limits<int>::max();
            atts.reset();
            while (atts.move_next())
            {
                auto& key = atts.element().key();
                if(key == "folder_path")
                {
                    m_folders.back().folder_path = atts.element().value();
                } 
                else if (key == "roi_inflation_percentage")
                {              
                    m_folders.back().roi_inflation_percentage = std::atoi(atts.element().value().c_str());
                }
                else if (key == "max_num_boxes")
                {
                    m_folders.back().max_num_boxes = std::atoi(atts.element().value().c_str());
                }
                else if (key == "expected_num_landmarks")
                {
                    m_folders.back().expected_num_landmarks = std::atoi(atts.element().value().c_str());
                }
                else if(key.find("segment_", 0) == 0)
                    m_folders.back().segments.push_back(std::atoi(atts.element().value().c_str()));
            }
            
            return;
        }
    }
 
    virtual void end_element ( 
        const unsigned long line_number,
        const std::string& name)
    {}

    virtual void characters ( 
        const std::string& data)
    {}

    virtual void processing_instruction (
        const unsigned long line_number,
        const std::string& target,
        const std::string& data)
    {}

private:
    std::vector<folder_tag> m_folders;
};
