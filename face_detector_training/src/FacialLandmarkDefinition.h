#include <dlib/geometry.h>
#include <dlib/image_processing.h>
#include <utility>
#include <vector>

enum FacialLandMark
{
    chin = 0,
    lcheek,
    lleye,
    lleyebrow,
    lmcheek,
    lmeyebrow,
    lmouth,
    lreye,
    lreyebrow,
    nosebridge,
    nosetip,
    rcheek,
    rleye,
    rleyebrow,
    rmcheek,
    rmeyebrow,
    rmouth,
    rreye,
    rreyebrow,
    topmouth
};


static std::vector<std::pair<dlib::point, dlib::point>> GetFacialLandMarkConnections(const dlib::full_object_detection& shape)
{
	std::vector<std::pair<dlib::point, dlib::point>> output(20);

	output.push_back(
		std::make_pair(
			shape.part(FacialLandMark::chin), 
			shape.part(FacialLandMark::lmcheek)));

	output.push_back(
		std::make_pair(
			shape.part(FacialLandMark::chin), 
			shape.part(FacialLandMark::rmcheek)));

	output.push_back(
		std::make_pair(
			shape.part(FacialLandMark::lcheek), 
			shape.part(FacialLandMark::lmcheek)));

	output.push_back(
		std::make_pair(
			shape.part(FacialLandMark::rcheek), 
			shape.part(FacialLandMark::rmcheek)));

	output.push_back(
		std::make_pair(
			shape.part(FacialLandMark::lcheek), 
			shape.part(FacialLandMark::lleyebrow)));

	output.push_back(
		std::make_pair(
			shape.part(FacialLandMark::rcheek), 
			shape.part(FacialLandMark::rreyebrow)));

	output.push_back(
		std::make_pair(
			shape.part(FacialLandMark::rmeyebrow), 
			shape.part(FacialLandMark::rreyebrow)));			

	output.push_back(
		std::make_pair(
			shape.part(FacialLandMark::lmeyebrow), 
			shape.part(FacialLandMark::lleyebrow)));

	output.push_back(
		std::make_pair(
			shape.part(FacialLandMark::lmeyebrow), 
			shape.part(FacialLandMark::lreyebrow)));

	output.push_back(
		std::make_pair(
			shape.part(FacialLandMark::rmeyebrow), 
			shape.part(FacialLandMark::rleyebrow)));

	output.push_back(
		std::make_pair(
			shape.part(FacialLandMark::nosebridge), 
			shape.part(FacialLandMark::lreyebrow)));

	output.push_back(
		std::make_pair(
			shape.part(FacialLandMark::nosebridge), 
			shape.part(FacialLandMark::rleyebrow)));

	output.push_back(
		std::make_pair(
			shape.part(FacialLandMark::nosebridge), 
			shape.part(FacialLandMark::lreye)));

	output.push_back(
		std::make_pair(
			shape.part(FacialLandMark::nosebridge), 
			shape.part(FacialLandMark::rleye)));

	output.push_back(
		std::make_pair(
			shape.part(FacialLandMark::lleye), 
			shape.part(FacialLandMark::lreye)));

	output.push_back(
		std::make_pair(
			shape.part(FacialLandMark::rreye), 
			shape.part(FacialLandMark::rleye)));

	output.push_back(
		std::make_pair(
			shape.part(FacialLandMark::nosebridge), 
			shape.part(FacialLandMark::nosetip)));

	output.push_back(
		std::make_pair(
			shape.part(FacialLandMark::topmouth), 
			shape.part(FacialLandMark::nosetip)));	

	output.push_back(
		std::make_pair(
			shape.part(FacialLandMark::topmouth), 
			shape.part(FacialLandMark::lmouth)));

	output.push_back(
		std::make_pair(
			shape.part(FacialLandMark::topmouth), 
			shape.part(FacialLandMark::rmouth)));

	return output;	
}