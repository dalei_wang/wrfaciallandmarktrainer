#include "XMLParser.h"
#include <utility>

std::ostream& operator<<(std::ostream& os, const part_tag& tag)  
{  
    os << "      <part name='"<< tag.name << "' x='" << tag.x << "' y='" << tag.y << "' />"<< std::endl;  
    return os;  
}  

std::ostream& operator<<(std::ostream& os, const box_tag& tag)  
{  
    os << "    <box top='" << tag.top << "' left='" << tag.left << "' width='" << tag.width << "' height='" << tag.height << "'>" << std::endl;
    for(auto& pt: tag.parts)
        os << pt;
    os << "    </box>" << std::endl;
    return os;  
} 

std::ostream& operator<<(std::ostream& os, const image_tag& tag)  
{  
    os << "  <image file='"<<tag.file_path << "'>" <<std::endl;
    for(auto& bx: tag.boxes)
        os << bx;
    os << "  </image>" <<std::endl;

    return os;  
} 

const std::unordered_map<std::string, image_tag>& FacialLandmarkXMLHandler::Get() const
{
    return m_images;
}

bool image_tag::Validate(int expectedNumLandmarks, int maxNumBoxes) const
{
    if(boxes.size() > maxNumBoxes)
    {
        std::cout<<file_path<<" has "<<boxes.size()<<" boxes when a max of "<<maxNumBoxes<<" is expected."<<std::endl;
        return false;
    }

    for(auto& box: boxes)
    {
        if(!box.Validate(file_path, expectedNumLandmarks))
            return false;
    }
    return true;
}

bool box_tag::Validate(std::string file_path, int expectedNumLandmarks) const
{
    if(width != height)
    {
        std::cout
            <<file_path
            <<" box ("
            <<left<<", "
            <<top<<", "
            <<width<<", "
            <<height<<") is not square."<<std::endl;
    }

    if(parts.size() != expectedNumLandmarks)
    {
        std::cout
            <<file_path
            <<" box ("
            <<left<<", "
            <<top<<", "
            <<width<<", "
            <<height<<") has "
            <<parts.size()<<" parts when "<<expectedNumLandmarks<<" are expected."<<std::endl;
    }

    return parts.size() == expectedNumLandmarks;
}

dlib::rectangle box_tag::FindTightestROI() const
{
    dlib::rectangle output = dlib::rectangle();

    output +=
        dlib::rectangle(
            left, 
            top, 
            left + width - 1, 
            top + height - 1);

    for(auto& part: parts)
        output += dlib::point(part.x, part.y);
    
    return output;
}

dlib::rectangle image_tag::FindTightestROI() const
{
    dlib::rectangle output = dlib::rectangle();
    
    for(auto& box: boxes)
        output += box.FindTightestROI();
    
    return output;
}

void part_tag::ShiftCoordinates(int dx, int dy)
{
    y += dy;
    x += dx;
}

void box_tag::ShiftCoordinates(int dx, int dy)
{
    top += dy;
    left += dx;

    for(auto& part: parts)
        part.ShiftCoordinates(dx, dy);
}

void image_tag::ShiftCoordinates(int dx, int dy)
{
    for(auto& box: boxes)
        box.ShiftCoordinates(dx, dy);   
}

void 
FacialLandmarkXMLHandler::end_element( 
    const unsigned long line_number,
    const std::string& name)
{
    if(name == "image")
    {
        m_currentImage = m_images.end();
    }
}

void 
FacialLandmarkXMLHandler::start_element( 
    const unsigned long line_number,
    const std::string& name,
    const dlib::attribute_list& atts)
{
    if(name == "image")
    {
        atts.reset();
        atts.move_next();
        if(atts.element().key() != "file")
            throw std::runtime_error("Unexpected tag");  

        std::string imgName = atts.element().value();;
        auto res = m_images.insert(std::make_pair(imgName, image_tag{imgName, {}}));
        
        if(!res.second) //duplicate image name
        {
            m_images.erase(res.first);
            m_currentImage = m_images.end();
            std::cout<<"Insersion of image: "<<imgName<<" failed. Possible duplicate."<<std::endl;
        }
        else
        {
            m_currentImage = res.first;
        }

        return;
    }

    if(name == "box")
    {
        if(m_currentImage != m_images.end())
        {
            (m_currentImage->second).boxes.push_back({0, 0, 0, 0, {}});
            atts.reset();
            while (atts.move_next())
            {
                auto& key = atts.element().key();
                if(key == "top")
                    (m_currentImage->second).boxes.back().top = std::atoi(atts.element().value().c_str());
                else if (key == "left")
                    (m_currentImage->second).boxes.back().left = std::atoi(atts.element().value().c_str());
                else if (key == "width")
                    (m_currentImage->second).boxes.back().width = std::atoi(atts.element().value().c_str());
                else if (key == "height")
                    (m_currentImage->second).boxes.back().height = std::atoi(atts.element().value().c_str());
            }
        }
        return;
    }

    if(name == "part")
    {
        if(m_currentImage != m_images.end())
        {
            (m_currentImage->second).boxes.back().parts.push_back({0, 0, ""});
            atts.reset();
            while (atts.move_next())
            {
                auto& key = atts.element().key();
                if(key == "name")
                    (m_currentImage->second).boxes.back().parts.back().name = atts.element().value();
                else if (key == "x")
                    (m_currentImage->second).boxes.back().parts.back().x = std::atoi(atts.element().value().c_str());
                else if (key == "y")
                    (m_currentImage->second).boxes.back().parts.back().y = std::atoi(atts.element().value().c_str());
            } 
        }
        return;
    }
}
