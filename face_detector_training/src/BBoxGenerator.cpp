
#include <iostream>
#include <dlib/dnn.h>
#include <dlib/data_io.h>
#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>
#include <sys/types.h>
#include <dirent.h>
#include <vector>
#include <cstring>
#include <string>
#include <cstdlib>
#include <fstream>
#include <stdexcept>
#include <cmath>
#include <sstream>
#include "XMLParser.h"

using namespace std;
using namespace dlib;

// ----------------------------------------------------------------------------------------

template <long num_filters, typename SUBNET> using con5d = con<num_filters,5,5,2,2,SUBNET>;
template <long num_filters, typename SUBNET> using con5  = con<num_filters,5,5,1,1,SUBNET>;

template <typename SUBNET> using downsampler  = relu<affine<con5d<32, relu<affine<con5d<32, relu<affine<con5d<16,SUBNET>>>>>>>>>;
template <typename SUBNET> using rcon5  = relu<affine<con5<45,SUBNET>>>;

using net_type = loss_mmod<con<1,9,9,1,1,rcon5<rcon5<rcon5<downsampler<input_rgb_image_pyramid<pyramid_down<6>>>>>>>>;

// ----------------------------------------------------------------------------------------

class TrainingXMLWriter
{
public:
	TrainingXMLWriter(const std::string& imgDir, const std::string& outputBaseName, unsigned int numBatches);
	void ProcessAllImages();

private:
	std::string m_imgDir;
	std::string m_outputBaseName;
	unsigned int m_numBatches;

	std::vector<std::string> m_imgFiles;
	std::vector<std::ofstream> m_outputXMLs;
	net_type m_net;

	void ListImageFiles();
	static int ImgFileFilter(const struct dirent* dir);

	void StartNewFile(unsigned int batchId);
	void CloseFile(unsigned int batchId);
	void ProcessBatch(unsigned int batchId);
};

void TrainingXMLWriter::StartNewFile(unsigned int batchId)
{
	std::stringstream fullOutputName;
	fullOutputName << m_imgDir << "/" << m_outputBaseName << "_" << batchId << ".xml";
	m_outputXMLs[batchId].open(fullOutputName.str(), std::ios::trunc);
	
	if(!m_outputXMLs[batchId].is_open())
	{
		std::stringstream errMsg;
		errMsg << "Could not open output xml file " << fullOutputName.str();
		throw std::runtime_error(errMsg.str());
	}

	m_outputXMLs[batchId] << "<?xml version='1.0' encoding='ISO-8859-1'?>"<<std::endl;
	m_outputXMLs[batchId] << "<?xml-stylesheet type='text/xsl' href='image_metadata_stylesheet.xsl'?>"<<std::endl;
	m_outputXMLs[batchId] << "<dataset>"<<std::endl;
	m_outputXMLs[batchId] << "<name>Training faces</name>"<<std::endl;
	m_outputXMLs[batchId] << "<comment>These are images from the PASCAL VOC 2011 dataset.</comment>"<<std::endl;
	m_outputXMLs[batchId] << "<images>"<<std::endl;
}

void TrainingXMLWriter::CloseFile(unsigned int batchId)
{
	if(m_outputXMLs[batchId].is_open())
	{
		m_outputXMLs[batchId] << "</images>" <<std::endl;
		m_outputXMLs[batchId] << "</dataset>" <<std::endl;
		m_outputXMLs[batchId].close();
	}
}

void TrainingXMLWriter::ProcessBatch(unsigned int batchId)
{
	auto batchSize = (m_imgFiles.size()+m_numBatches-1)/m_numBatches;
	auto startIndex = batchId*batchSize;
	auto endIndex = std::min(startIndex+batchSize-1, m_imgFiles.size()-1);

	if(endIndex >= startIndex)
	{
		StartNewFile(batchId);
		for(auto i = startIndex; i <= endIndex; ++i)  	
		{
			image_tag imgTag;
			imgTag.file_path = m_imgFiles[i];

			auto fullImgPath = m_imgDir + "/" + m_imgFiles[i];
			dlib::matrix<dlib::rgb_pixel> img;
		    dlib::load_image(img, fullImgPath);

			double originalWidth = static_cast<double>(img.nc());
			double originalHeight = static_cast<double>(img.nr());
	
			while(img.size() < 1800*1800)
				dlib::pyramid_up(img);

			double resizedWidth = static_cast<double>(img.nc());
			double resizedHeight = static_cast<double>(img.nr());

			auto dets = m_net(img);
		
			double widthRatio = originalWidth/resizedWidth;
			double heightRatio = originalHeight/resizedHeight;

			for (auto&& d : dets)
			{
				auto top = std::lround(d.rect.top()*heightRatio);
				auto left = std::lround(d.rect.left()*widthRatio);
				auto height = std::lround(d.rect.height()*heightRatio);
				auto width = std::lround(d.rect.width()*widthRatio);

				imgTag.boxes.push_back(box_tag{top, left, width, height, {}});
			}
			m_outputXMLs[batchId] <<imgTag;
		}
		CloseFile(batchId);
	}
}

int TrainingXMLWriter::ImgFileFilter(const struct dirent* dir)
{
	const char *s = dir->d_name;
	int len = static_cast<int>(strlen(s)) - 4;
	if(len >= 0)
	{
		if(strncmp(s + len, ".jpg", 4) == 0 
		|| strncmp(s + len, ".png", 4) == 0
		|| strncmp(s + len, ".gif", 4) == 0
		|| strncmp(s + len, ".bmp", 4) == 0)
			return 1;
	}
	return 0;
}

void TrainingXMLWriter::ListImageFiles()
{
    DIR* dirp = opendir(m_imgDir.c_str());
    struct dirent **namelist;

	int numFiles = scandir(m_imgDir.c_str(), &namelist, TrainingXMLWriter::ImgFileFilter, alphasort);
	if(numFiles>0)
	{
		m_imgFiles.reserve(numFiles);
		for(int i = 0; i < numFiles; ++i)
		{	
			m_imgFiles.push_back(std::string(namelist[i]->d_name));
			free(namelist[i]);
		}
	}
    closedir(dirp);
}

TrainingXMLWriter::TrainingXMLWriter(const std::string& imgDir, const std::string& outputBaseName, unsigned int numBatches)
: m_imgDir(imgDir)
, m_outputBaseName(outputBaseName)
, m_numBatches(numBatches)
{
	m_outputXMLs.resize(numBatches);
	dlib::deserialize("./mmod_human_face_detector.dat") >> m_net;
	ListImageFiles();
}

void TrainingXMLWriter::ProcessAllImages()
{
	for(unsigned int batch = 0; batch < m_numBatches; ++batch)
		ProcessBatch(batch);
}

int main(int argc, char** argv) try
{
	if (argc != 4) 
		throw std::runtime_error("Usage: ./BBoxGenerator path-to-image-folder(string) output-label-file-without-extension(string) max-#-of-batches(integer)");    

	TrainingXMLWriter xmlOutput(argv[1], argv[2], std::atoi(argv[3]));
	xmlOutput.ProcessAllImages();
}
catch(std::exception& e)
{
    cout << e.what() << endl;
}


