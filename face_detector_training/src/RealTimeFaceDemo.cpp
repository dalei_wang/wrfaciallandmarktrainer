
#include <dlib/opencv.h>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <dlib/dnn.h>
#include <dlib/data_io.h>
#include <dlib/gui_widgets/widgets.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing/frontal_face_detector.h>
#include "FacialLandmarkDefinition.h"

using namespace std;
using namespace dlib;

// ----------------------------------------------------------------------------------------

template <long num_filters, typename SUBNET> using con5d = con<num_filters,5,5,2,2,SUBNET>;
template <long num_filters, typename SUBNET> using con5  = con<num_filters,5,5,1,1,SUBNET>;

template <typename SUBNET> using downsampler  = relu<affine<con5d<32, relu<affine<con5d<32, relu<affine<con5d<16,SUBNET>>>>>>>>>;
template <typename SUBNET> using rcon5  = relu<affine<con5<45,SUBNET>>>;

using net_type = loss_mmod<con<1,9,9,1,1,rcon5<rcon5<rcon5<downsampler<input_rgb_image_pyramid<pyramid_down<6>>>>>>>>;

// ----------------------------------------------------------------------------------------

void DlibHOGFaceDetector()
{
    try
    {
        cv::VideoCapture cap(0);
        if (!cap.isOpened())
        {
 			std::runtime_error("Unable to connect to camera");
        }

        image_window win;

        // Load face detection and pose estimation models.
        frontal_face_detector detector = get_frontal_face_detector();
        shape_predictor pose_model;
        deserialize("./shape_predictor_68_face_landmarks.dat") >> pose_model;

        // Grab and process frames until the main window is closed by the user.
        while(!win.is_closed())
        {
            // Grab a frame
            cv::Mat temp;
            if (!cap.read(temp))
            {
                break;
            }

			cv::flip(temp, temp, 1);

            // Turn OpenCV's Mat into something dlib can deal with.  Note that this just
            // wraps the Mat object, it doesn't copy anything.  So cimg is only valid as
            // long as temp is valid.  Also don't do anything to temp that would cause it
            // to reallocate the memory which stores the image as that will make cimg
            // contain dangling pointers.  This basically means you shouldn't modify temp
            // while using cimg.
            cv_image<bgr_pixel> cimg(temp);

            // Detect faces 
            std::vector<rectangle> faces = detector(cimg);

            for (unsigned long i = 0; i < faces.size(); ++i)
            {
		        rectangle r(
		                    (long)faces[i].left(),
		                    (long)faces[i].top(),
		                    (long)faces[i].right(),
		                    (long)faces[i].bottom()
		                    );

				draw_rectangle(cimg, r, rgb_pixel{0, 0, 255}, 2);
			}
            // Find the pose of each face.
            std::vector<full_object_detection> shapes;
            for (unsigned long i = 0; i < faces.size(); ++i)
                shapes.push_back(pose_model(cimg, faces[i]));

            // Display it all on the screen
            win.clear_overlay();
            win.set_image(cimg);
            win.add_overlay(render_face_detections(shapes));
        }
    }
    catch(serialization_error& e)
    {
        cout << "You need dlib's default face landmarking model file to run this example." << endl;
        cout << "You can get it from the following URL: " << endl;
        cout << "   http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2" << endl;
        cout << endl << e.what() << endl;
    }
    catch(exception& e)
    {
        cout << e.what() << endl;
    }
}

void DlibCNNFaceDetector(const std::string& landmarkModel)
{
    net_type net;
    deserialize("./mmod_human_face_detector.dat") >> net;  

    try
    {
	   	cv::VideoCapture cap(0);
		if (!cap.isOpened())
		{
		    std::runtime_error("Unable to connect to camera");
		}

		image_window win;
        shape_predictor pose_model;
		deserialize(landmarkModel.c_str()) >> pose_model;
        //deserialize("./shape_predictor_5_face_landmarks.dat") >> pose_model;

        // Grab and process frames until the main window is closed by the user.
        while(!win.is_closed())
        {
            // Grab a frame
            cv::Mat temp;
            if (!cap.read(temp))
            {
                break;
            }

			cv::flip(temp, temp, 1);
            // Turn OpenCV's Mat into something dlib can deal with.  Note that this just
            // wraps the Mat object, it doesn't copy anything.  So cimg is only valid as
            // long as temp is valid.  Also don't do anything to temp that would cause it
            // to reallocate the memory which stores the image as that will make cimg
            // contain dangling pointers.  This basically means you shouldn't modify temp
            // while using cimg.
            cv_image<bgr_pixel> cvimg(temp);

			matrix<rgb_pixel> img;
    		assign_image(img, cvimg);

		    // Upsampling the image will allow us to detect smaller faces but will cause the
		    // program to use more RAM and run longer.
		    //while(img.size() < 1024*1024)
		    //    pyramid_up(img);

		    // Note that you can process a bunch of images in a std::vector at once and it runs
		    // much faster, since this will form mini-batches of images and therefore get
		    // better parallelism out of your GPU hardware.  However, all the images must be
		    // the same size.  To avoid this requirement on images being the same size we
		    // process them individually in this example.
		    std::vector<dlib::mmod_rect> dets = net(img);
			std::vector<full_object_detection> shapes;

			for (auto&& d : dets)
			{
				auto facialFeatures = pose_model(cvimg, d.rect); 
				shapes.push_back(facialFeatures);
			}

		    win.clear_overlay();
		    win.set_image(img);
		    for (auto&& d : dets)
		        win.add_overlay(d);

			for(auto&& shape: shapes)
			{
				for(unsigned int i = 0; i<shape.num_parts(); ++i)
				{
					auto center = shape.part(i);
					win.add_overlay(
						dlib::image_display::overlay_circle(
							center, 
							5, 
							dlib::rgb_pixel(255,0,0), 
							std::to_string(i)
						)
					);
				}
                
                if(shape.num_parts() == 20) //For the wrnch 20 point model only
                {
                    auto lines = GetFacialLandMarkConnections(shape);

                    for(auto& line: lines)
                    {
                        win.add_overlay(
                            dlib::image_display::overlay_line(
                                line.first, 
                                line.second, 
                                dlib::rgb_pixel(0,255,0)
                            )
                        );   
                    }
                }
			}
		}
	}
	catch(std::exception& e)
	{
		cout << e.what() << endl;
	}
}

int main(int argc, char** argv) try
{
	//DlibHOGFaceDetector();

	if(argc == 2)
	{
		DlibCNNFaceDetector(argv[1]);
	}
	else
	{
		throw std::runtime_error("Usage: ./RealTimeFaceDemo path-to-landmark-detection-model");    
	}
}
catch(std::exception& e)
{
    cout << e.what() << endl;
}
