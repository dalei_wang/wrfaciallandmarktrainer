cmake_minimum_required(VERSION 3.2.1)
set(CMAKE_CXX_STANDARD 14)

project(face_detector_training)

set(XML_SOURCES 
	src/XMLParser.cpp
)

set(MERGE_SOURCES 
	src/MergeXMLs.cpp
)

set(BBOX_SOURCES 
	src/BBoxGenerator.cpp
)

set(FIX_BBOX_SOURCES 
	src/FixBBox.cpp
)

set(DEMO_SOURCES 
	src/RealTimeFaceDemo.cpp
)

set(TRAIN_SOURCES 
	src/TrainShapePredictor.cpp
)

find_library(DLIB dlib PATHS /usr/local/lib)
find_library(OCV_CORE opencv_core PATHS /usr/local/lib)
find_library(OCV_HIGH_GUI opencv_highgui PATHS /usr/local/lib)
find_library(OCV_IMPROC opencv_imgproc PATHS /usr/local/lib)
find_library(OCV_VIDEO opencv_video PATHS /usr/local/lib)
find_library(OCV_VIDEOIO opencv_videoio PATHS /usr/local/lib)
find_library(BLAS blas PATHS /usr/lib)
find_library(LAPACK lapack PATHS /usr/lib)

add_library(XML_Parser ${XML_SOURCES})
target_link_libraries(XML_Parser PUBLIC ${DLIB})

add_executable(MergeXMLs ${MERGE_SOURCES})
target_link_libraries(MergeXMLs PUBLIC XML_Parser)

add_executable(BBoxGenerator ${BBOX_SOURCES})
target_link_libraries(BBoxGenerator PUBLIC ${DLIB} XML_Parser)

add_executable(FixBBox ${FIX_BBOX_SOURCES})
target_link_libraries(FixBBox PUBLIC ${DLIB} XML_Parser)

add_executable(TrainShapePredictor ${TRAIN_SOURCES})
target_link_libraries(TrainShapePredictor PUBLIC ${DLIB} ${BLAS} ${LAPACK})

add_executable(RealTimeFaceDemo ${DEMO_SOURCES})
target_link_libraries(RealTimeFaceDemo PUBLIC ${DLIB} ${OCV_CORE} ${OCV_HIGH_GUI} ${OCV_IMPROC} ${OCV_VIDEOIO} ${BLAS} ${LAPACK})

add_custom_command (
	TARGET RealTimeFaceDemo
	POST_BUILD
	COMMAND cp ${PROJECT_SOURCE_DIR}/models/*.dat ${PROJECT_BINARY_DIR}
)
